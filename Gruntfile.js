/*jslint node: true*/
module.exports = function (grunt) {
    'use strict';

    grunt.loadNpmTasks('grunt-jslint');
    grunt.loadNpmTasks('grunt-shipit');
    grunt.loadNpmTasks('shipit-git-update');
    grunt.loadNpmTasks('grunt-jsonlint');
    grunt.loadNpmTasks('grunt-fixpack');
    grunt.loadNpmTasks('shipit-composer-simple');

    grunt.initConfig({
        jslint: {
            Gruntfile: {
                src: 'Gruntfile.js'
            }
        },
        shipit: {
            options: {
                composer: {
                    noDev: true
                }
            },
            staging: {
                deployTo: '/home/devanimafac/public_html/',
                servers: 'devanimafac@tools.animafac.net',
                branch: 'develop'
            }
        },
        jsonlint: {
            manifests: {
                src: ['*.json'],
                options: {
                    format: true
                }
            }
        },
        fixpack: {
            package: {
                src: 'package.json'
            }
        }
    });

    grunt.registerTask('lint', ['jslint', 'jsonlint', 'fixpack']);
    grunt.registerTask('staging', ['shipit:staging', 'update', 'composer:install']);
};
