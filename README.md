# animafac-wordpress

WordPress setup used on animafac.net

## Setup

WordPress plugins are installed with [Composer](https://getcomposer.org/):

```bash
composer install
```

## How it works

Composer will install WordPress in `wordpress/`, and plugins and themes in `wp-content/`.
It will also install some static assets (mainly HTML pages used to promote events) in other subfolders.

We use Composer instead of the native WordPress updates so we can test plugin updates before deploying them on the production server.

The repository contains various symlinks (`wp-includes/`, `wp-login.php`, etc.) to make WordPress think it is installed to the root of the repository. This is necessary to maintain compatibility with some old plugins with hardcoded paths.

## Grunt tasks

[Grunt](https://gruntjs.com/) can be used to run some automated tasks defined in `Gruntfile.js`.

Your first need to install JavaScript dependencies with [Yarn](https://yarnpkg.com/):

```bash
yarn install
```

### Lint

You can check that the JavaScript and JSON files are formatted correctly:

```bash
grunt lint
```

### Shipit

The `staging` task uses [Shipit](https://github.com/shipitjs/shipit) to install the latest develop branch on the staging server:

```bash
grunt staging
```

## CI

[Gitlab CI](https://docs.gitlab.com/ee/ci/) is used to run a task that warns if minor updates to dependencies are available.
This is useful if you don't want to miss a security update for a WordPress plugin.
